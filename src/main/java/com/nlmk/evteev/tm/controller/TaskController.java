package com.nlmk.evteev.tm.controller;

import com.nlmk.evteev.tm.entity.Task;
import com.nlmk.evteev.tm.exceptions.TaskNotFoundException;
import com.nlmk.evteev.tm.service.TaskService;
import com.nlmk.evteev.tm.utils.TaskManagerUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

/**
 * Класс контроллера задач.
 */
public class TaskController extends AbstractController {

    private final Logger logger = LogManager.getLogger(TaskController.class);
    private final TaskService taskService;

    /**
     * Конструктор класса контроллера задач
     *
     * @param taskService сервисный класс задач {@link TaskService}
     */
    protected TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    /**
     * Изменение задачи по индексу
     *
     * @return код исполнения
     */
    public int updateTaskByIndex() throws TaskNotFoundException {
        logger.info("[Update task by index]");
        System.out.println("Введите индекс задачи: ");
        final int vId = Integer.parseInt(scanner.nextLine()) - 1;
        final Task task;
        try {
            task = taskService.findByIndex(vId);
            System.out.println("Введите новое название задачи: ");
            final String name = scanner.nextLine();
            System.out.println("Введите новое описание задачи: ");
            final String description = scanner.nextLine();
            if (taskService.update(task.getId(), name, description) != null) {
                return TaskManagerUtil.printAndReturnOk();
            } else {
                return TaskManagerUtil.printAndReturnFail("Изменение задачи не удалось.");
            }
        } catch (TaskNotFoundException e) {
            logger.error(e);
            throw e;
        }
    }

    /**
     * Изменение задачи по коду
     *
     * @return код исполнения
     */
    public int updateTaskById() {
        logger.info("[Update task by id]");
        System.out.println("Введите код задачи: ");
        final Long vId = Long.getLong(scanner.nextLine());
        System.out.println("Введите новое название задачи: ");
        final String name = scanner.nextLine();
        System.out.println("Введите новое описание задачи: ");
        final String description = scanner.nextLine();
        if (taskService.update(vId, name, description) != null) {
            return TaskManagerUtil.printAndReturnOk();
        } else {
            return TaskManagerUtil.printAndReturnFail("Изменение задачи не удалось!");
        }
    }

    /**
     * Удаление задачи по названию
     *
     * @return код исполнения
     */
    public int removeTaskByName() {
        logger.info("[Remove task by name]");
        System.out.println("Введите имя задачи: ");
        final String name = scanner.nextLine();
        final Task task = taskService.removeByName(name);
        return task == null
                ? TaskManagerUtil.printAndReturnFail("Удаление зада не удалось!")
                : TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Удаление задачи по коду
     *
     * @return код исполнения
     */
    public int removeTaskById() {
        System.out.println("[Remove task by id]");
        System.out.println("Введите ID задачи: ");
        final Long vId = scanner.nextLong();
        final Task task = taskService.removeById(vId);
        return task == null
                ? TaskManagerUtil.printAndReturnFail("Удаление зада не удалось!")
                : TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Удаление задачи по индексу
     *
     * @return код исполнения
     */
    public int removeTaskByIndex() {
        System.out.println("[Remove task by index]");
        System.out.println("Введите индекс задачи: ");
        final Integer vId = scanner.nextInt();
        final Task task = taskService.removeByIndex(vId);
        return task == null
                ? TaskManagerUtil.printAndReturnFail("Удаление зада не удалось!")
                : TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Просмотр задачи в консоли
     *
     * @param task задача {@link Task}
     */
    public void viewTask(final Task task) {
        if (task == null) return;
        logger.info("[View Task]");
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("PROJECT_ID: " + task.getProjectId());
        logger.info("[OK]");
    }

    /**
     * Просмотр задачи по индексу
     *
     * @return код исполнения
     */
    public int viewTaskByIndex() throws TaskNotFoundException {
        System.out.println("Введите индекс задачи: ");
        final int index = scanner.nextInt() - 1;
        final Task task;
        try {
            task = taskService.findByIndex(index);
            viewTask(task);
            return TaskManagerUtil.printAndReturnOk();
        } catch (TaskNotFoundException e) {
            logger.error(e);
            throw e;
        }
    }

    /**
     * Создание задачи
     *
     * @return код выполнения
     */
    public int createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("Укажите имя задачи: ");
        final String lv_name = scanner.nextLine();
        taskService.create(lv_name);
        return TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Очистка задачи
     *
     * @return код выполнения
     */
    public int clearTask() {
        logger.info("[CLEAR TASK]");
        taskService.clear();
        return TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Список задач
     *
     * @return код выполнения
     */
    public int listTask() {
        System.out.println("[LIST TASK]");
        System.out.println(taskService.findAll());
        return TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Поиск задачи по ID
     *
     * @param taskId код задачи
     * @return задача {@link Task}
     */
    public Task findTaskById(final Long taskId) {
        if (taskId == null) return null;
        return taskService.findById(taskId);
    }

    /**
     * Список задач, привязанных к проекту
     *
     * @return код исполнения
     */
    public int findAllByProjectId() {
        System.out.println("[VIEW TASKS BY PROJECT ID]");
        System.out.println("Введите код проекта: ");
        final Long projectId = Long.parseLong(scanner.nextLine());
        if (projectId == null) return 0;
        List<Task> tasks = taskService.findAllByProjectId(projectId);
        if (tasks.isEmpty()) {
            return TaskManagerUtil.printAndReturnFail("Задач для проекта не найдено");
        } else {
            tasks.forEach(task -> viewTask(task));
        }
        return TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Список задач, непривязанных к проектам
     *
     * @return код исполнения
     */
    public int findAllWithoutProject() {
        System.out.println("[VIEW TASKS WITHOUT PROJECT]");
        List<Task> tasks = taskService.findAllWithoutProject();
        if (tasks.isEmpty()) {
            return TaskManagerUtil.printAndReturnFail("Задач, непривязанных к проектам не найдено");
        } else {
            tasks.forEach(task -> viewTask(task));
        }
        return TaskManagerUtil.printAndReturnOk();
    }

    /**
     * Удаление задачи из проекта
     *
     * @param taskId код задачи
     * @return код выполнения
     */
    public int removeTaskFromProject(final Long taskId) {
        if (taskId == null) return 0;
        taskService.removeTaskFromProject(taskId);
        return TaskManagerUtil.printAndReturnOk();
    }

}
