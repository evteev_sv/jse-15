package com.nlmk.evteev.tm.service;

import com.nlmk.evteev.tm.entity.Project;
import com.nlmk.evteev.tm.exceptions.ProjectNotFoundException;
import com.nlmk.evteev.tm.repository.ProjectRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.UUID;

/**
 * Сервисный класс для работы с репозиторием проектов
 */
public class ProjectService {

    private static Logger logger = LogManager.getLogger(ProjectService.class);
    private ProjectRepository projectRepository;

    /**
     * Конструктор класса
     *
     * @param projectRepository репозиторий проектов
     */
    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    /**
     * Создание проекта и добавление его в список проектов
     *
     * @param name   имя проекта
     * @param userId код пользователя {@link UUID}
     * @return объект типа {@link Project}
     */
    public Project create(final String name, final UUID userId) {
        if (name == null || name.isEmpty())
            return null;
        logger.trace("Создание проекта с названием " + name + " для пользователя " + userId.toString());
        return projectRepository.create(name, userId);
    }

    /**
     * Очистка списка проектов
     */
    public void clear() {
        projectRepository.clear();
    }

    /**
     * Очистка списка на основании другого списка проектов
     *
     * @param projectList список проектов для очистки {@link List}
     */
    public void clearListProject(List<Project> projectList) {
        projectRepository.clearListProject(projectList);
    }

    /**
     * Возвращает список проектов
     *
     * @return список проектов {@link List}
     */
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    /**
     * Поиск проекта по индексу
     *
     * @param index индекс проекта
     * @return проект {@link Project}
     * @throws ProjectNotFoundException
     */
    public Project findByIndex(final int index) throws ProjectNotFoundException {
        if (index < 0) return null;
        Project project = projectRepository.findByIndex(index);
        if (project == null){
            logger.error("Проект по данному индексу не найден!");
            throw new ProjectNotFoundException("Проект по данному индексу не найден!");
        }
        return project;
    }

    /**
     * Поиск проекта по наименованию
     *
     * @param name наименование проекта
     * @return проект {@link Project}
     */
    public Project findByName(final String name) {
        if (name == null || name.isEmpty())
            return null;
        logger.trace("Поиск проекта с названием " + name);
        return projectRepository.findByName(name);
    }

    /**
     * Поиск проектов по ID пользователя
     *
     * @param userId код пользователя
     * @return список пользовательских проектов {@link List}
     * @see Project
     */
    public List<Project> findByUserId(UUID userId) {
        logger.trace("Поиск проектов по пользователю " + userId.toString());
        return projectRepository.findByUserId(userId);
    }

    /**
     * Поиск проекта по коду
     *
     * @param id код проекта
     * @return проект {@link Project}
     */
    public Project findById(final Long id) {
        if (id == null) return null;
        return projectRepository.findById(id);
    }

    /**
     * Удаление проекта по коду
     *
     * @param id код проекта
     * @return проект, который был удален {@link Project}
     */
    public Project removeById(final Long id) {
        if (id == null) return null;
        return projectRepository.removeById(id);
    }

    /**
     * Удален епроекта по индексу
     *
     * @param index индекс проекта
     * @return проект, который был удален {@link Project}
     */
    public Project removeByIndex(final int index) {
        if (index < 0 || index > projectRepository.size() - 1) return null;
        return projectRepository.removeByIndex(index);
    }

    /**
     * Удаление проекта по названию.
     *
     * @param name название проекта
     * @return проект, который был удален {@link Project}
     */
    public Project removeByName(final String name) {
        if (name == null || name.isEmpty())
            return null;
        return projectRepository.removeByName(name);
    }

    /**
     * Изменение проекта
     *
     * @param id          код проекта
     * @param name        наименование проекта
     * @param description описание проекта
     * @return проект, который изменен {@link Project}
     */
    public Project update(final Long id, final String name, final String description) {
        if (id == null || name == null || name.isEmpty() || description == null)
            return null;
        return projectRepository.update(id, name, description);
    }

    /**
     * Изменение владельца проекта
     *
     * @param projectId код проекта
     * @param userId    код владельца
     * @return проект {@link Project}
     */
    public Project updateProjectOwner(Long projectId, UUID userId) {
        if (projectId == null || userId == null) return null;
        return projectRepository.updateProjectOwner(projectId, userId);
    }

    /**
     * Возвращает размер коллекции проектов
     *
     * @return размер коллекции
     */
    public int size() {
        return projectRepository.size();
    }

}
